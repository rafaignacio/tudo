# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include grupo05_eureka
class grupo05_eureka {
  group { 'java':
       ensure => 'present',
       gid    => '501',
     }

  user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }

  file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }

  file { '/opt/apps/eureka':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
  package { 'java-1.8.0-openjdk-devel':
        ensure => installed,
        name   => $java,
    }


  exec { 'eureka-get':
    command => "/usr/bin/wget -q http://23.96.48.125/artfactory/grupo5/eureka/eureka.jar -O /opt/apps/eureka/eureka.jar",
    creates => "/opt/apps/eureka/eureka.jar" 
  }

  exec { 'start eureka':
      cwd => '/opt/apps/eureka/',
      command => '/usr/bin/java -jar -javaagent:/opt/appdynamics/javaagent.jar eureka.jar &',
      path => '/usr/bin/:/bin/',
  }
  
  
  service { 'firewalld':
    ensure => 'stopped',
    enable => 'false'
  }


  file { '/opt/apps/eureka/eureka.jar':
    mode => '0755',
    require => Exec[ 'eureka-get', 'start eureka' ]
  }

 

}
