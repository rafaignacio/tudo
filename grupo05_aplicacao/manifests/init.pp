class grupo05_aplicacao {

  exec { 'refresh_aplicacao':
    command     => "/usr/bin/systemctl restart grupo05_aplicacao.service",
    refreshonly => true,
  }

  exec { 'refresh_daemon':
    command     => "/usr/bin/systemctl daemon-reload",
    refreshonly => true,
  }

  group { 'java':
    ensure => 'present',
    gid    => '501',
  }

  user { 'java':
    ensure           => 'present',
    gid              => '501',
    home             => '/home/java',
    password         => '!!',
    password_max_age => '99999',
    password_min_age => '0',
    shell            => '/bin/bash',
    uid              => '501',
  }

  file { ['/opt/apps', '/opt/apps/grupo05', '/opt/apps/grupo05/aplicacao']:
    ensure => directory,
    owner  => 'java',
    group  => 'java',
  }

  file { "/etc/systemd/system/grupo05_aplicacao.service":
    mode => "0644",
    owner => 'root',
    group => 'root',
    source => 'puppet:///modules/grupo05_aplicacao/grupo05_aplicacao.service',
    notify => Exec['refresh_daemon'],
  }

  package { 'maven':
    ensure => installed,
    name   => $maven,
  }

  remote_file { '/opt/apps/grupo05/aplicacao/grupo05_aplicacao.jar':
    ensure => latest,
    owner => 'java',
    group => 'java',
    source => 'http://23.96.48.125/artfactory/grupo5/aplicacao/aplicacao.jar',
    notify => Exec['refresh_aplicacao'],
  }
}
