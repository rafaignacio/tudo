# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include grupo05_cliente
class grupo05_cliente {
  # A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include cliente
  group { 'java':
    ensure => 'present',
    gid    => '501',
  }

  user { 'java':
    ensure           => 'present',
    gid              => '501',
    home             => '/home/java',
    password         => '!!',
    password_max_age => '99999',
    password_min_age => '0',
    shell            => '/bin/bash',
    uid              => '501',
  }
  
  file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
  }

  file { '/opt/apps/cliente':
        ensure => directory,
        owner => 'java',
        group => 'java',
  }


  package { 'java-1.8.0-openjdk-devel':
    ensure => installed,
  }

  exec { 'cliente-get':
    command => "/usr/bin/wget -q http://23.96.48.125/artfactory/grupo5/cliente/cliente.jar -O /opt/apps/cliente/cliente.jar",
    creates => "/opt/apps/cliente/cliente.jar" 
  }

  service { 'firewalld':
    ensure => 'stopped',
    enable => 'false'
  }
  
  exec { 'run app':
    cwd => "/opt/apps/cliente",
    command => "/usr/bin/java -jar -javaagent:/opt/appdynamics/javaagent.jar cliente.jar &",
    path => '/usr/bin/:/bin/'
  }
  
  file { '/opt/apps/cliente/cliente.jar':
    mode => '0755',
    require => Exec[ 'cliente-get', 'run app' ]
  }  
  
}
